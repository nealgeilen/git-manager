﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Git_Manager.src.Projects;
using Git_Manager.src.Updaters;
using Git_Manager.src.Updaters.Pip;

namespace Git_Manager
{
    /// <summary>
    /// Interaction logic for ProjectDetail.xaml
    /// </summary>
    public partial class ProjectDetail : Window
    {
        AbstractProject item;
        string type;

        public ProjectDetail(dynamic item)
        {
            InitializeComponent();

            this.item = item;
            this.type = item.GetType().ToString();

            this.LoadPachagesList();
            this.LoadGitInfo();
        }

        protected void LoadPachagesList()
        {
            switch (this.type)
            {
                case "Git_Manager.src.Projects.Php":
                    Php project = (Php)this.item;
                    PackagListView.ItemsSource = Composer.getAvailableUpdates(project);

                    break;
                case "Git_Manager.src.Projects.Python":
                    Python projectPython = (Python)this.item;
                    PackagListView.ItemsSource = Pip.getOutdatedPackages(projectPython);
                    //(Python)project
                    break;
            }
        }

        protected void LoadGitInfo()
        {
            BranchesListVieuw.ItemsSource = this.item.Repo.Branches;
            lblAuthor.Text = this.item.LastCommit.Author.Name;
            //lblBranch.Text = this.item.LastCommit
            lblMessage.Text = this.item.LastCommit.Message;
            lblName.Text = this.item.Name;
        }


        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            dynamic i = item.DataContext;
            //(new ProjectDetail(item.DataContext)).Show();
            MessageBoxResult result = MessageBox.Show("Would you like to update: " + i.name, "Update", MessageBoxButton.YesNoCancel);

            if(MessageBoxResult.Yes == result)
            {
                switch (this.type)
                {
                    case "Git_Manager.src.Projects.Php":
                        Php project = (Php)this.item;
                        Composer.UpdatePackage(project, i.name);
                        MessageBox.Show(i.name + " Updated");


                        break;
                    case "Git_Manager.src.Projects.Python":
                        Python projectPython = (Python)this.item;
                        Pip.UpdatePackage(projectPython, i.name);
                        MessageBox.Show(i.name + " Updated");
                        break;
                }
                this.LoadPachagesList();
            }
        }



    }
}
