﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Git_Manager.src.Projects;
using Git_Manager.src.Updaters.Pip;

namespace Git_Manager.src
{
    class Loader
    {
        public static List<AbstractProject> SearchGit()
        {
            //Define list where all projects will be defined
            List<AbstractProject> Projects = new List<AbstractProject> { };
            //Loop thourg directory
            foreach (string dir in Directory.EnumerateDirectories(@"p:\", "*", SearchOption.TopDirectoryOnly))
            {
                Php projectPhp = new Php(dir);

                //Check if there is a composer file and git. When there is. It is a Php project
                if (projectPhp.HasComposer && projectPhp.HasGit)
                {
                    Projects.Add(projectPhp);
                    //When project is added to list. Continue to next directory and skip next code
                    continue;
                }

                Python projectPython = new Python(dir);

                //Check if there is a PythonCach directory and git. When there is. It is a Python project
                if (projectPython.HasCache && projectPython.HasGit)
                {
                    Projects.Add(projectPython);
                    //When project is added to list. Continue to next directory and skip next code
                    continue;
                }

                Csharp projectCsharp = new Csharp(dir);

                //Check if there is a Visual studio direcory and git. When there is. It is a C# project
                if (projectCsharp.HasVs && projectCsharp.HasGit)
                {
                    Projects.Add(projectCsharp);
                    //When project is added to list. Continue to next directory and skip next code
                    continue;
                }
            }
            return Projects;
        }


        public static void fillListvieuw(ListView list)
        {
            List<AbstractProject> projects = Loader.SearchGit();
            list.ItemsSource = projects;
        }
    }
}
