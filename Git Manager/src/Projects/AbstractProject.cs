﻿using System;
using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Git_Manager.src.Projects
{
    abstract class AbstractProject
    {

        /**
         * Load deatils into propperties
        **/
        protected void load()
        {
            this.HasGit = (Directory.Exists(this.Dir + @"\.git"));
            if (this.HasGit)
            {
                this.Repo = new Repository(this.Dir);
                this.Name = Path.GetFileName(this.Dir);
                this.LastCommit = this.Repo.Head.Tip;
            }

        }

        /*
         Getters and Setters Abstarct Class
             */

        public override string ToString()
        {
            return this.Name + " " + this.GetType().ToString();
        }
        private bool _hasgit;
        public bool HasGit { get => _hasgit; protected set => _hasgit = value; }
        private string _dir;
        public string Dir { get => _dir; protected set => _dir = value; }
        private string _name;
        public string Name { get => _name; protected set => _name = value; }
        private Repository _repo;
        public Repository Repo { get => _repo; protected set => _repo = value; }
        private Commit _lastcommit;
        public Commit LastCommit { get => _lastcommit; protected set => _lastcommit = value; }
    }
}
