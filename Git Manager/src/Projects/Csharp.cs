﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Git_Manager.src.Projects
{
    class Csharp : AbstractProject
    {
        private bool _hasvs;
        public bool HasVs { get => _hasvs; protected set => _hasvs = value; }

        public Csharp(string Dir)
        {
            this.Dir = Dir;
            this.load();
            this.HasVs = Directory.Exists(this.Dir + @"\.vs");
        }

        public Csharp(){}
    }
}
