﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Git_Manager.src.Projects
{
    class Php : AbstractProject 
    {
        private bool _hascomposer;
        public bool HasComposer { get => _hascomposer; protected set => _hascomposer = value; }

        public Php(string Dir)
        {
            this.Dir = Dir;
            this.load();
            this.HasComposer = File.Exists(this.Dir + @"\composer.json");
        }

        public Php(){}

    }
}
