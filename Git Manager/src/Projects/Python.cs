﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Git_Manager.src.Projects
{
    class Python : AbstractProject
    {
        private bool _hascache;
        public bool HasCache { get => _hascache; protected set => _hascache = value; }

        public Python(string Dir)
        {
            this.Dir = Dir;
            this.load();
            this.HasCache = Directory.Exists(this.Dir + @"\__pycache__");
        }

        public Python(){}

    }
}
