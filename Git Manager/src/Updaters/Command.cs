﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Git_Manager.src
{
    class Command
    {
        //Command proccess
        protected Process Process;
        //command what needts to fire
        public string CommandCmd { get; set; }
        //Working directory where the command needts to fire
        public string Dir { get; set; }




        public Command(string Command = "echo Oscar", string Dir = null)
        {

            this.CommandCmd = Command;
            this.Dir = Dir;
            this.setup();
        }

        public Command()
        {
            this.CommandCmd = "";
            this.Dir = null;
            this.Process = null;
        }

        public void setup()
        {
            this.Process = new Process();
            //Set program that will execute command
            this.Process.StartInfo.FileName = "cmd.exe";
            this.Process.StartInfo.RedirectStandardInput = true;
            this.Process.StartInfo.RedirectStandardOutput = true;
            //Make shure there will not open an command prop to the user. 
            this.Process.StartInfo.CreateNoWindow = true;
            this.Process.StartInfo.UseShellExecute = false;
            //Set working directory
            this.Process.StartInfo.WorkingDirectory = this.Dir;
        }

        public String Execute()
        {
            //Chek if a command has been set. When not return an empty string back; 
            if(this.Process != null)
            {
                //Start proccess
                this.Process.Start();
                //write line into command shell
                this.Process.StandardInput.WriteLine(this.CommandCmd);

                //Close shell
                this.Process.StandardInput.Flush();
                this.Process.StandardInput.Close();

                //read outcome from start to finish
                string result = this.Process.StandardOutput.ReadToEnd();

                //Remove spaces form result and soft new lines. 
                //When split on a hard new line. The result of the command whil show in the 5th item in the array; 
                string[] results = result.Replace("\n", "").Replace(" ", "").Trim().Split('\r');

                return results[4];
            }
            return "";
        }

        
    }
}
