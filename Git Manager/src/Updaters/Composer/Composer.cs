﻿using Git_Manager.src.Projects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Git_Manager.src.Updaters
{
    class Composer
    {
        public static List<ComposerPackage> getAvailableUpdates(Php pro)
        {
            /*
             Command build up: 
             "Composer" = program, 
             "show" = list of installed packages, 
             "-o" = list ouddated pages, 
             "--format=json" = format the result of the command as a json string, 
             "--direct" = list packages installed in working directory
             */
            String result = (new Command("composer show -o --format=json --direct", pro.Dir)).Execute();
            //Get result of command and deserilize to object. Objects is defined in src/Updaters/Composer/ComposerPackages.cs 
            Root DeserializedClass = JsonConvert.DeserializeObject<Root>(result);
            return DeserializedClass.installed;
        }

        public static string UpdatePackage(Php pro, string packageName = "")
        {
            return (new Command("composer update " + packageName, pro.Dir)).Execute();
        }
    }
}
