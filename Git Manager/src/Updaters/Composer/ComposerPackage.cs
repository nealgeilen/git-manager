﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Git_Manager.src
{

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ComposerPackage
    {
        public string name { get; set; }
        public string version { get; set; }
        public string latest { get; set; }
        [JsonProperty("latest-status")]
        public string LatestStatus { get; set; }
        public string description { get; set; }
    }

    public class Root
    {
        public List<ComposerPackage> installed { get; set; }
    }
}
