﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Git_Manager.src.Projects;
using Newtonsoft.Json;

namespace Git_Manager.src.Updaters.Pip
{
    class Pip
    {
        public static List<PipPackage> getOutdatedPackages(Python pro)
        {
            /*
            Command build up: 
            "pip" = program, 
            "list" = list of installed packages, 
            "-o" = list ouddated pages, 
            "-l" = list packages installed in working directory
            "--format json" = format the result of the command as a json string, 
            */
            string result = (new Command("pip list -o  -l --format json", pro.Dir)).Execute();
            return JsonConvert.DeserializeObject<List<PipPackage>>(result);
        }

        public static String UpdatePackage(Python project, string package)
        {
            return (new Command("pip install "+package+" --upgrade", project.Dir)).Execute();
        }

    }
}
