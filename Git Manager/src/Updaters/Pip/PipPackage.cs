﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Git_Manager.src.Updaters.Pip
{
    public class PipPackage
    {
        public string name { get; set; }
        public string version { get; set; }
    }
}
