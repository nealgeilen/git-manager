# Git Manager / Package updater
For al the various project I have on my desktop, It has always been a hassle tho have everything updated. 

Now with this application, I wanted to ease the pain of staying up to date.
All my project are stored in the same directory. And Python and Php are my most frequently used programming languages.
And for both, there is a neet packages system. 
So here in this project, I wanted to combine the two systems. 
Both are command-line programs. 

So in short. The following program is a GUI for Git information and updating composer and pip packages



This project is strictly for my personal use.

## Application
At the moment there are two programming language supported

- Python and pip packages
- Php and composer packages

For both packages managers, it is required to have a global presence on your system. The commands "pip" and "composer" need to be in working order.




## Fontys Leerdoelen (dutch)
Voor de fontys verdieping wil ik even wat stukken code aanhalen. 
Deze kunnen de leerdoelen of gewoon functies zijn waar ik tevreden over ben.     

### AbstractProject class
[Bestand](https://gitlab.com/nealgeilen/git-manager/-/blob/master/Git%20Manager/src/Projects/AbstractProject.cs)
### Command class
[Bestand](https://gitlab.com/nealgeilen/git-manager/-/blob/master/Git%20Manager/src/Updaters/Command.cs)

### Documentatie

Voor de classes is er een [UML klassen diagram](https://gitlab.com/nealgeilen/git-manager/-/blob/master/classes-uml.pdf) gemaakt.

### Meer
Buiten dit project heb ik in de verdieping nog aan anderen projecten gewerkt. 
Echter lever ik dit project als "wedstrijd" in. 
Dit komt omdat ik niet voldoende de eisen van de verdieping kan laten zien in de anderen projecten.
Het wilt echt niet zeggen dat daar niks in staat wat niet noemenswaardig is. 
Dus voor aanvulling op dit project heb ik tevens nog het onderstaande.

[smartpot.nealgeilen.nl](https://github.com/NealGeilen/smartpot.nl)

[smartpot android app](https://github.com/NealGeilen/SmartPot.Android)


